<?php
  include("V_cabecalho.php");


	$linhas = mysqli_query($MySQLi, 'SELECT * FROM artigo');
	    		while($artigo = mysqli_fetch_array($linhas)):
            $editora=$artigo['id_editora'];
            $id_artigo=$artigo['id']

				?>

		    	<div class="card" style="width: 69rem;">
  
  					<div class="card-body">
    					<h5 class="card-title"><?php echo $artigo['nome'] ?></h5><br>
              <?php

                $consulta = $infos_bd->prepare("SELECT * FROM editora WHERE id = :id");
                $consulta->bindValue(":id", $editora);
                $consulta->execute();
                $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
                foreach ($resultado as $key=> $value)
                {
                  if ($key == "nome"){
              ?>
              <p class="card-text">Editora que o publicou: <?php echo $value ?></p>
              <?php }} ?>
    					<p class="card-text"><?php echo $artigo['data_publicacao'] ?></p>
    					<button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">Excluir</button><br>

    					<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  							<div class="modal-dialog" role="document">
    							<div class="modal-content">
      								<div class="modal-header">
        								<h5 class="modal-title" id="exampleModalLabel">Cuidado!</h5>
        								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          								<span aria-hidden="true">&times;</span>
        								</button>
      								</div>
      								<div class="modal-body">
       									Você tem certeza que deseja excluir esse item?
      								</div>
      								<div class="modal-footer">
        								<a href="C_deletar_artigo.php?id=<?php echo $artigo['id'] ?>">Excluir</button><br><br>
        								<button type="button" class="btn btn-outline-info" data-dismiss="modal">Cancelar</button>
      								</div>
    							</div>
  							</div>
						</div>

    					<a href="V_alterar_artigo.php?id=<?php echo $artigo['id'] ?>">Editar</a>
  					</div>
				</div>
				<br>

				<?php endwhile ?>

				</div>

			<br>
		
<?php 
	include("V_rodape.php");
?>

