<?php
	include("V_cabecalho.php");
?>

<br>
	<form action="C_inserir_autoria.php" method='get'>
		<p>Artigo:<br>
		<div class="form-row align-items-center">
      		<select class="custom-select mr-sm-2" id="artigo" name="artigo">
      			<option selected>Selecionar</option>

				<?php

					$linhas = mysqli_query($MySQLi, 'SELECT * FROM  artigo');
	    			while($artigo = mysqli_fetch_array($linhas)):
              			$id = $artigo['id'];

				?>
				<option value="<?php echo $artigo['id']; ?>"><?php echo $artigo['nome']; ?></option>
				<?php endwhile ?>
			</select>
		</div>
		<p>Autor(a):<br>  				
		<div class="form-row align-items-center">
      		<select class="custom-select mr-sm-2" id="autor" name="autor">
      			<option selected>Selecionar</option>

				<?php

					$linhas = mysqli_query($MySQLi, 'SELECT * FROM  autor');
	    			while($autor = mysqli_fetch_array($linhas)):
              			$id = $autor['id'];

				?>
				<option value="<?php echo $autor['id']; ?>"><?php echo $autor['nome']; ?></option>
				<?php endwhile ?>
			</select>
		</div>
		<br>
		<button	type="submit" class="btn btn-outline-danger">
		Salvar
		</button>
	</form>

<?php 
	include("V_rodape.php");
?>
