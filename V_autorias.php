<?php
	include("V_cabecalho.php");
	$linhas = mysqli_query($MySQLi, 'SELECT * FROM  
		artigo_autor');
	while($artigo_autor = mysqli_fetch_array($linhas)):
        $id = $artigo_autor['id'];
        $id_autor = $artigo_autor['id_autor'];
        $id_artigo = $artigo_autor['id_artigo'];
?>
	<div class="card" style="width: 69rem;">
  
  		<div class="card-body">
    					
<?php
        $consulta = $infos_bd->prepare("SELECT * FROM autor WHERE id = :id");
        $consulta->bindValue(":id", $id_autor);
        $consulta->execute();
        $resultado = $consulta->fetch(PDO::FETCH_ASSOC);
        foreach ($resultado as $key=> $value){
            if ($key == "nome"){
?>
            	<p class="card-text">Autor: <?php echo $value ?></p>
        <?php }} 
        $consulta2 = $infos_bd->prepare("SELECT * FROM artigo WHERE id = :id");
        $consulta2->bindValue(":id", $id_artigo);
        $consulta2->execute();
        $resultado2 = $consulta2->fetch(PDO::FETCH_ASSOC);
        foreach ($resultado2 as $key=> $value){
            if ($key == "nome"){
?>
            	<p class="card-text">Obra: <?php echo $value ?></p>

            	<button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">Excluir</button><br>

    					<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  							<div class="modal-dialog" role="document">
    							<div class="modal-content">
      								<div class="modal-header">
        								<h5 class="modal-title" id="exampleModalLabel">Cuidado!</h5>
        								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          								<span aria-hidden="true">&times;</span>
        								</button>
      								</div>
      								<div class="modal-body">
       									Você tem certeza que deseja excluir esse item?
      								</div>
      								<div class="modal-footer">
        								<a href="C_deletar_autoria.php?id=<?php echo $id ?>">Excluir</button><br><br>
        								<button type="button" class="btn btn-outline-info" data-dismiss="modal">Cancelar</button>
      								</div>
    							</div>
  							</div>
						</div>

    					<a href="V_alterar_autoria.php?id=<?php echo $id ?>">Editar</a>
  			</div>
		</div>
		<br>
<?php }} 
    endwhile;
	include("V_rodape.php");
?>



