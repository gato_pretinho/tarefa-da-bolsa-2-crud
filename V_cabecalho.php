<?php 
	include("conexão.php");
  header('Content-type: text/html; charset=iso-8859-1');
?>
<!DOCTYPE html>
    <html lang="pt-br">
    <head>
        <meta charset=iso-8859-1>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    </head>
    <body>
    	<style>
            #fundo {
                background-color: #98f74a;
            }
        </style>

        <div class="container" id="fundo">

        	<nav class="navbar navbar-expand-lg navbar-light bg-light">
  				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    				<span class="navbar-toggler-icon"></span>
  				</button>
  				<div class="collapse navbar-collapse" id="navbarNavDropdown">
    				<ul class="navbar-nav">
      					<li class="nav-item dropdown">
        					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         					Artigos
        					</a>
        					<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          						<a class="dropdown-item" href="index.php">Todos</a>
          						<a class="dropdown-item" href="V_inserir_artigo.php">Adicionar artigo</a>
        					</div>
      					</li>
      					<li class="nav-item dropdown">
        					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         					Autores
        					</a>
        					<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          						<a class="dropdown-item" href="V_autores.php">Todos</a>
          						<a class="dropdown-item" href="V_inserir_autor.php">Adicionar autor</a>
        					</div>
      					</li>
      					<li class="nav-item dropdown">
        					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         					Editora
        					</a>
        					<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          						<a class="dropdown-item" href="V_editoras.php">Todas</a>
          						<a class="dropdown-item" href="V_inserir_editora">Adicionar editora</a>
        					</div>
      					</li>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Autoria
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                      <a class="dropdown-item" href="V_autorias.php">Todas</a>
                      <a class="dropdown-item" href="V_inserir_autoria">Adicionar autoria</a>
                  </div>
                </li>
    				</ul>
  				</div>
			</nav>
        	<br>