-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Tempo de geração: 22-Set-2022 às 21:37
-- Versão do servidor: 10.4.10-MariaDB
-- versão do PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `tarefa_2_chaves_estrangeiras`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `artigo`
--

DROP TABLE IF EXISTS `artigo`;
CREATE TABLE IF NOT EXISTS `artigo` (
  `nome` varchar(300) NOT NULL,
  `id_editora` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_publicacao` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_editora` (`id_editora`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `artigo`
--

INSERT INTO `artigo` (`nome`, `id_editora`, `id`, `data_publicacao`) VALUES
('Estágio na Escola Estadual de Educação Fundamental Ibirubá', 1, 1, '2021-07-14');

-- --------------------------------------------------------

--
-- Estrutura da tabela `artigo_autor`
--

DROP TABLE IF EXISTS `artigo_autor`;
CREATE TABLE IF NOT EXISTS `artigo_autor` (
  `id_artigo` int(11) NOT NULL,
  `id_autor` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `id_autor` (`id_autor`),
  KEY `id_artigo` (`id_artigo`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `artigo_autor`
--

INSERT INTO `artigo_autor` (`id_artigo`, `id_autor`, `id`) VALUES
(1, 2, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `autor`
--

DROP TABLE IF EXISTS `autor`;
CREATE TABLE IF NOT EXISTS `autor` (
  `nome` varchar(300) NOT NULL,
  `biografia` varchar(2000) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `autor`
--

INSERT INTO `autor` (`nome`, `biografia`, `id`) VALUES
('Laila Francieli Schumann', 'Estudante de História.', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `editora`
--

DROP TABLE IF EXISTS `editora`;
CREATE TABLE IF NOT EXISTS `editora` (
  `nome` varchar(300) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(2000) DEFAULT NULL,
  `telefone` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `editora`
--

INSERT INTO `editora` (`nome`, `id`, `descricao`, `telefone`) VALUES
('Desconhecida', 1, 'O artigo em questï¿½o nï¿½o possui editora ou ela ï¿½ desconhecida.', 19000),
('UFMG', 2, 'Editora da Universidade Federal de Minas Gerais', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
