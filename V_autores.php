<?php
	include("V_cabecalho.php");
?>        	

<h5><i>Procura algu�m em espec�fico?</i></h5>
<br>
<form action="V_pesquisar_autores.php" method='get'>
  <div class="row">
    <div class="col-10">
      Nome:<br>
      <input name="nome" type="text" class="form-control">
    </div>
    <div class="col-2">
      <br>
      <button type="submit" class="btn btn-outline-info">
      Pesquisar
      </button>
    </div>
  </div>
</form>
<br>

<h3><i>Todos os autores no sistema:</i></h3>
        		<?php

					$linhas = mysqli_query($MySQLi, 'SELECT * FROM autor');
	    		while($autor = mysqli_fetch_array($linhas)):

				?>

		    	<div class="card" style="width: 69rem;">
  
  					<div class="card-body">
    					<h5 class="card-title"><?php echo $autor['nome'] ?></h5><br>
    					<p class="card-text"><?php echo $autor['biografia'] ?></p>
    					<button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">Excluir</button><br>

    					<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  							<div class="modal-dialog" role="document">
    							<div class="modal-content">
      								<div class="modal-header">
        								<h5 class="modal-title" id="exampleModalLabel">Cuidado!</h5>
        								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          								<span aria-hidden="true">&times;</span>
        								</button>
      								</div>
      								<div class="modal-body">
       									Voc� tem certeza que deseja excluir esse item?
      								</div>
      								<div class="modal-footer">
        								<a href="C_deletar_autor.php?id=<?php echo $autor['id'] ?>">Excluir</button><br><br>
        								<button type="button" class="btn btn-outline-info" data-dismiss="modal">Cancelar</button>
      								</div>
    							</div>
  							</div>
						</div>

    					<a href="V_alterar_autor.php?id=<?php echo $autor['id'] ?>">Editar</a>
  					</div>
				</div>
				<br>

				<?php endwhile ?>

				</div>

			<br>
		
<?php 
	include("V_rodape.php");
?>