<?php
	include("V_cabecalho.php");


					$linhas = mysqli_query($MySQLi, 'SELECT * FROM  editora');
	    		while($editora = mysqli_fetch_array($linhas)):
              $id = $editora['id'];

				?>

		    	<div class="card" style="width: 69rem;">
  
  					<div class="card-body">
    					<h5 class="card-title"><?php echo $editora['nome'] ?></h5><br>
    					<p class="card-text"><?php echo $editora['descricao'] ?></p>
    					<button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal">Excluir</button><br>

    					<div class="modal fade" id="exampleModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  							<div class="modal-dialog" role="document">
    							<div class="modal-content">
      								<div class="modal-header">
        								<h5 class="modal-title" id="exampleModalLabel">Cuidado!</h5>
        								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          								<span aria-hidden="true">&times;</span>
        								</button>
      								</div>
      								<div class="modal-body">
       									Você tem certeza que deseja excluir esse item?
      								</div>
      								<div class="modal-footer">
        								<a href="C_deletar_editora.php?id=<?php echo $id; ?>">Excluir</button><br><br>
        								<button type="button" class="btn btn-outline-info" data-dismiss="modal">Cancelar</button>
      								</div>
    							</div>
  							</div>
						</div>

    					<a href="V_alterar_editora.php?id=<?php echo $id; ?>">Editar</a>
  					</div>
				</div>
				<br>

				<?php endwhile ?>

				</div>

			<br>
		
<?php 
	include("V_rodape.php");
?>