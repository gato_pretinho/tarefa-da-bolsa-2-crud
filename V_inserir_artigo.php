<?php
	include("V_cabecalho.php");
?>
	<br>
	<form action="C_inserir_artigo.php" method='get'>
		<p>Nome:<br>
		<input name="nome" type="text" class="form-control"></p>
		<p>Editora que o publicou:<br>  				
			<div class="form-row align-items-center">
      				<select class="custom-select mr-sm-2" id="editora" name="editora">
      					<option selected>Selecionar</option>

						<?php

							$linhas = mysqli_query($MySQLi, 'SELECT * FROM  editora');
	    					while($editora = mysqli_fetch_array($linhas)):
              					$id = $editora['id'];

						?>
							<option value="<?php echo $editora['id']; ?>"><?php echo $editora['nome']; ?></option>
						<?php endwhile ?>
					</select>
			</div>
			<br>
			<button	type="submit" class="btn btn-outline-danger">
			Salvar
			</button>
	</form>
<?php 
	include("V_rodape.php");
?>
